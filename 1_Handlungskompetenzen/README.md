# Handlungskompetenzen

## Kompetenzmatrix

| Kompetenzenband | RP | HZ | Novizenkompetenz | Fortgeschrittene Kompetenz | Kompetenz professionellen Handelns | Kompetenzexpertise |
|----------|----|----|------------------|----------------------------|------------------------------------|--------------------|
| **a) Linux Namespaces**                               |                                                                                      |          |                                                                                      |                                                                                        |                                                                                          |                                                                                                                                     |
| Verstehen, wie Linux Namespaces zur Isolation von Prozessen verwendet werden | B4.6, B12.1, A3.3 | 1 | Kennt die grundlegenden Konzepte von Namespaces und deren Zweck | Kann einfache Namespaces konfigurieren und anwenden | Kann Namespaces in komplexen Szenarien zur Prozessisolation effektiv einsetzen | Entwickelt und optimiert Namespaces für maximale Sicherheit und Effizienz |
| Die verschiedenen Arten von Namespaces und deren spezifische Funktionen kennen | B4.6, B12.1, A3.3 | 1 | Kennt die verschiedenen Arten von Namespaces (PID, NET, MOUNT, IPC, UTS, USER) | Kann die spezifischen Funktionen und Anwendungsfälle der verschiedenen Namespaces erklären | Setzt die verschiedenen Namespaces gezielt ein, um spezifische Anforderungen zu erfüllen | Berät und schult andere in der effektiven Nutzung und Optimierung von Namespaces |
| Praktische Anwendung von Namespaces zur Verbesserung der Sicherheit und Effizienz von Containern | B4.6, B9.1, B9.3, A3.3 | 1 | Kann einfache Beispiele zur Anwendung von Namespaces nachvollziehen | Konfiguriert Namespaces zur Verbesserung der Sicherheit und Effizienz in typischen Szenarien | Implementiert Namespaces in Produktionsumgebungen und überwacht deren Effektivität | Entwickelt Best Practices und Sicherheitsrichtlinien für die Nutzung von Namespaces in grossen Umgebungen |
| **b) Container Sicherheit**                               |                                                                                      |          |                                                                                      |                                                                                        |                                                                                          |                                                                                                                                     |
| Die grundlegenden Sicherheitsprinzipien für Container verstehen | B9.1, B9.3, A3.3 | 2 | Kennt die grundlegenden Sicherheitsprinzipien und deren Bedeutung | Kann grundlegende Sicherheitsmassnahmen in Container-Umgebungen umsetzen | Implementiert umfassende Sicherheitsstrategien und -massnahmen in Produktionsumgebungen | Entwickelt und optimiert Sicherheitsstrategien für Container-Umgebungen und berät andere |
| Sicherheitsrisiken in Container-Umgebungen identifizieren und minimieren können | B9.1, B9.3, A3.3 | 2 | Kann einfache Sicherheitsrisiken erkennen und grundlegende Massnahmen zur Minimierung ergreifen | Identifiziert und bewertet Sicherheitsrisiken in verschiedenen Szenarien und setzt geeignete Massnahmen um | Entwickelt und implementiert umfassende Sicherheitspläne zur Risikominimierung | Führt Sicherheitsaudits durch und entwickelt fortschrittliche Sicherheitslösungen für komplexe Umgebungen |
| Best Practices zur Sicherung von Containern anwenden können | B9.1, B9.3, A3.3 | 2 | Kennt grundlegende Best Practices zur Sicherung von Containern | Setzt Best Practices in typischen Szenarien um und überprüft deren Wirksamkeit | Implementiert und überwacht Best Practices in Produktionsumgebungen und passt diese bei Bedarf an | Entwickelt neue Best Practices und schult andere in deren Anwendung |
| **PodMan**                               |                                                                                      |          |                                                                                      |                                                                                        |                                                                                          |                                                                                                                                     |
| **c) Chapter 1: Introduction and Overview of Containers**                               |                                                                                      |          |                                                                                      |                                                                                        |                                                                                          |                                                                                                                                     |
| Verstehen, wie Container die Anwendungsentwicklung erleichtern | B4.6, A2.5, A3.3 | 3 | Kennt die grundlegenden Konzepte von Containern | Kann erklären, wie Container die Anwendungsentwicklung erleichtern | Setzt Container in Entwicklungsprojekten effektiv ein | Entwickelt Strategien zur Optimierung der Anwendungsentwicklung mit Containern |
| Die Unterschiede zwischen Containern und virtuellen Maschinen kennen | B4.6, A2.6 | 3 | Kennt die grundlegenden Unterschiede zwischen Containern und VMs | Kann die Unterschiede anhand von Beispielen erklären | Wählt je nach Anwendungsfall zwischen Containern und VMs | Berät und schult andere in der optimalen Nutzung von Containern und VMs |
| Die Grundlagen der Container-Orchestrierung und die Funktionen von Red Hat OpenShift verstehen | B4.6, B12.1, A3.3 | 3 | Kennt die grundlegenden Konzepte der Container-Orchestrierung | Kann die Funktionen von Red Hat OpenShift erklären | Setzt OpenShift zur Orchestrierung von Containern ein | Entwickelt und optimiert Orchestrierungsstrategien mit OpenShift |
| **d) Chapter 2: Podman Basics**                               |                                                                                      |          |                                                                                      |                                                                                        |                                                                                          |                                                                                                                                     |
| Die grundlegende Nutzung von Podman zur Erstellung und Verwaltung von Containern erlernen | B4.6, A2.5, A3.4 | 4 | Kennt die grundlegenden Befehle von Podman | Kann einfache Container mit Podman erstellen und verwalten | Verwendet Podman zur Verwaltung komplexer Container-Umgebungen | Entwickelt und optimiert Podman-basierte Workflows |
| Die Grundlagen der Container-Netzwerke verstehen | B4.6, B12.3, A3.3 | 4 | Kennt die grundlegenden Konzepte der Container-Netzwerke | Kann Container-Netzwerke konfigurieren und verwalten | Setzt Container-Netzwerke in Produktionsumgebungen ein | Entwickelt und optimiert Netzwerkstrategien für Container |
| Den Lebenszyklus von Containern managen können | B4.6, B12.4, A3.4 | 4 | Kennt die grundlegenden Schritte des Container-Lebenszyklus | Kann den Lebenszyklus von Containern verwalten | Verwendet fortgeschrittene Techniken zur Verwaltung des Container-Lebenszyklus | Entwickelt und optimiert Lebenszyklus-Management-Strategien für Container |
| **e) Chapter 3: Container Images**                               |                                                                                      |          |                                                                                      |                                                                                        |                                                                                          |                                                                                                                                     |
| Container-Images in verschiedenen Registries finden und verwalten können | B4.6, A2.5, A3.3 | 5 | Kennt die grundlegenden Konzepte von Container-Registries | Kann Container-Images in Registries finden und verwalten | Verwendet Registries zur Verwaltung von Container-Images in Produktionsumgebungen | Entwickelt und optimiert Strategien zur Verwaltung von Container-Images |
| Container-Images herunterladen und managen können | B4.6, A2.5, A3.4 | 5 | Kennt die grundlegenden Befehle zum Herunterladen von Container-Images | Kann Container-Images herunterladen und verwalten | Verwendet fortgeschrittene Techniken zur Verwaltung von Container-Images | Entwickelt und optimiert Workflows zur Verwaltung von Container-Images |
| Sensible Daten in Container-Images identifizieren und vermeiden können | B9.1, B9.3, A3.3 | 5 | Kennt die grundlegenden Sicherheitsprinzipien für Container-Images | Kann Container-Images auf sensible Daten überprüfen | Verwendet Techniken zur Sicherstellung der Datensicherheit in Container-Images | Entwickelt und optimiert Sicherheitsstrategien für Container-Images |
| **f) Chapter 4: Custom Container Images**                               |                                                                                      |          |                                                                                      |                                                                                        |                                                                                          |                                                                                                                                     |
| Benutzerdefinierte Container-Images zur Containerisierung von Anwendungen erstellen können | B4.6, A2.5, A3.4 | 6 | Kennt die grundlegenden Befehle zur Erstellung von Container-Images | Kann einfache Container-Images erstellen | Verwendet fortgeschrittene Techniken zur Erstellung von Container-Images | Entwickelt und optimiert Strategien zur Erstellung von Container-Images |
| Ein Containerfile mit grundlegenden Befehlen und Best Practices schreiben können | B4.6, A2.5, A3.3 | 6 | Kennt die grundlegenden Befehle für Containerfiles | Kann ein Containerfile mit Best Practices schreiben | Verwendet fortgeschrittene Techniken zur Erstellung von Containerfiles | Entwickelt und optimiert Best Practices für Containerfiles |
| Rootlose Container mit Podman ausführen können | B9.1, B9.3, A3.3 | 6 | Kennt die grundlegenden Konzepte von rootlosen Containern | Kann rootlose Container mit Podman ausführen | Verwendet rootlose Container in Produktionsumgebungen | Entwickelt und optimiert Strategien für rootlose Container |
| **g) Chapter 5: Persisting Data**                               |                                                                                      |          |                                                                                      |                                                                                        |                                                                                          |                                                                                                                                     |
| Datenbank-Container mit Persistenz ausführen können | B12.3, B12.4, A3.3 | 7 | Kennt die grundlegenden Konzepte der Datenpersistenz | Kann Datenbank-Container mit Persistenz ausführen | Verwendet fortgeschrittene Techniken zur Datenpersistenz in Containern | Entwickelt und optimiert Strategien zur Datenpersistenz in Containern |
| Den Prozess des Einbindens von Volumes und deren häufige Anwendungsfälle verstehen | B12.3, B12.4, A3.3 | 7 | Kennt die grundlegenden Konzepte des Volume-Einbindens | Kann Volumes einbinden und deren Anwendungsfälle erklären | Verwendet Volumes zur Datenpersistenz in Produktionsumgebungen | Entwickelt und optimiert Strategien zur Nutzung von Volumes |
| Containerisierte Datenbanken erstellen können | B12.3, B12.4, A3.3 | 7 | Kennt die grundlegenden Konzepte der containerisierten Datenbanken | Kann einfache containerisierte Datenbanken erstellen | Verwendet fortgeschrittene Techniken zur Erstellung von containerisierten Datenbanken | Entwickelt und optimiert Strategien zur Erstellung von containerisierten Datenbanken |
| **h) Chapter 6: Troubleshooting Containers**                               |                                                                                      |          |                                                                                      |                                                                                        |                                                                                          |                                                                                                                                     |
| Container-Logs analysieren und einen Remote-Debugger konfigurieren können | B14.2, A2.1, A3.3 | 8 | Kennt die grundlegenden Konzepte der Log-Analyse | Kann Container-Logs analysieren und einfache Fehler beheben | Verwendet fortgeschrittene Techniken zur Fehlerbehebung und Debugging | Entwickelt und optimiert Strategien zur Fehlerbehebung und Debugging |
| Häufige Container-Probleme beheben können | B14.2, A2.1, A3.3 | 8 | Kennt die häufigsten Container-Probleme | Kann häufige Container-Probleme identifizieren und beheben | Verwendet fortgeschrittene Techniken zur Behebung von Container-Problemen | Entwickelt und optimiert Strategien zur Behebung von Container-Problemen |
| Einen Remote-Debugger während der Anwendungsentwicklung konfigurieren können | B14.2, A2.1, A3.3 | 8 | Kennt die grundlegenden Konzepte des Remote-Debuggings | Kann einen Remote-Debugger konfigurieren und verwenden | Verwendet fortgeschrittene Techniken zur Konfiguration und Nutzung von Remote-Debuggern | Entwickelt und optimiert Strategien zur Nutzung von Remote-Debuggern |
| **i) Chapter 7: Multi-container Applications with Compose**                               |                                                                                      |          |                                                                                      |                                                                                        |                                                                                          |                                                                                                                                     |
| Multi-Container-Anwendungen mit Podman Compose ausführen können | B4.6, A2.5, A3.3 | 9 | Kennt die grundlegenden Konzepte von Podman Compose | Kann einfache Multi-Container-Anwendungen mit Podman Compose ausführen | Verwendet fortgeschrittene Techniken zur Verwaltung von Multi-Container-Anwendungen | Entwickelt und optimiert Strategien zur Nutzung von Podman Compose |
| Die häufigen Anwendungsfälle von Compose verstehen | B4.6, A2.5, A3.3 | 9 | Kennt die häufigsten Anwendungsfälle von Compose | Kann Compose in typischen Szenarien anwenden | Verwendet Compose zur Verwaltung komplexer Anwendungen | Entwickelt und optimiert Strategien zur Nutzung von Compose |
| Eine wiederholbare Entwicklerumgebung mit Compose konfigurieren können | B4.6, A2.5, A3.3 | 9 | Kennt die grundlegenden Konzepte der Entwicklerumgebung mit Compose |

* RP = Rahmenlehrplan
* HZ = Handlungsziele

# Rahmenlehrplan

## Modulspezifische Handlungskompetenzen

 * B4 Entwicklungsmethoden zur Lösung von ICT-Problemen und Entwicklung von ICT-Innovationen zielführend einsetzen
   * B4.6 Aktuelle technologiebasierte Entwicklungswerkzeuge einsetzen (Niveau: 3)
   
 * B9 Datenschutz und Datensicherheit gewährleisten
   * B9.1 ICT-Sicherheitskonzepte zur Gewährleistung der ICT-Sicherheit wie Datenschutz, Datensicherheit und Verfügbarkeit einhalten, umsetzen und unterhalten (Niveau: 3)
   * B9.3 Sicherheitsrelevante Bausteine vernetzter ICTInfrastrukturen identifizieren, die Gefährdungslage beurteilen und geeignete organisatorische, personelle, infrastrukturelle und technische Schutzmassnahmen ableiten (Niveau: 3)   

 * B12 System- und Netzwerkarchitektur bestimmen
   * B12.1 Die bestehende Systemarchitektur beurteilen und weiterentwickeln (Niveau: 3)
   * B12.3 Bestehende ICT Konfigurationen analysieren, Umsetzungsvarianten für die Erweiterung definieren und Soll-Konfigurationen entwickeln (Niveau: 3)
   * B12.4 Die Anforderungen an ein Konfigurationsmanagementsystem einer ICT-Organisation erheben und mögliche Lösungsvarianten vorschlagen (Niveau: 3)   

 * B14 Konzepte und Services umsetzen
   * B14.1 Technische und organisatorische Massnahmen planen und für die Einführung von Software bzw. Releases ausarbeiten (Niveau: 3)
   * B14.2 Probleme und Fehler im operativen Betrieb überwachen, identifizieren, zuordnen, beheben oder falls erforderlich eskalieren (Niveau: 3)  

## Allgemeine Handlungskompetenzen

 * A2 Kommunikation situationsangepasst und wirkungsvoll gestalten
   * A2.1 Mündlich wie schriftlich sachlogisch, transparent und klar kommunizieren
   * A2.3 Quantität und Qualität der Informationen adressatengerecht selektieren und daraus folgend die Art und Form der Information festlegen
   * A2.5 Informations- und Kommunikationstechnologien (ICT) professionell einsetzen und etablieren
   * A2.6 Die branchenspezifischen Fachtermini des Engineerings verwenden und diese adressatengerecht kommunizieren

 * A3 Die persönliche Entwicklung reflektieren und aktiv gestalten
   * A3.1 Die eigenen Kompetenzen bezüglich der beruflichen Anforderungen regelmässig reflektieren, bewerten und daraus den Lernbedarf ermitteln
   * A3.2 Neues Wissen mit geeigneten Methoden erschliessen und arbeitsplatznahe Weiterbildung realisieren
   * A3.3 Neue Technologien kritisch reflexiv beurteilen, adaptieren und integrieren
   * A3.4 Die eigenen digitalen Kompetenzen kontinuierlich weiterentwickeln
   * A3.5 Das eigene Denken, Fühlen und Handeln reflektieren und geeignete persönliche Entwicklungsmassnahmen definieren und umsetzen      

## Anforderungsniveau

Das Anforderungsniveau einer Kompetenz ist durch die Komplexität der zu lösenden Problemstellung, die Veränderlichkeit und Unvorhersehbarkeit des Arbeitskontextes und die Verantwortlichkeit im Bereich der Zusammenarbeit und Führung definiert. HF Absolvierende sind generell in der Lage Problemstellungen und Herausforderungen zu analysieren, diese adäquat zu bewerten und mit innovativen Problemlösestrategien zu lösen. Die Handlungskompetenzen werden in vier Anforderungsniveaus eingestuft.

### Kompetenzniveau 1: Novizenkompetenz

Erfüllen selbständig fachliche Anforderungen; mehrheitlich wiederkehrende Aufgaben in einem überschaubaren und stabil strukturierten Tätigkeitsgebiet; Arbeit im Team und unter Anleitung.

### Kompetenzniveau 2: fortgeschrittene Kompetenz

Erkennen und analysieren umfassende fachliche Aufgabenstellungen in einem komplexen Arbeitskontext und sich veränderndem Arbeitsbereich; führen teils kleinere Teams; erledigen die Arbeiten selbständig unter Verantwortung einer Drittperson.

### Kompetenzniveau 3: Kompetenz professionellen Handelns

Bearbeiten neue komplexe Aufgaben und Problemstellungen in einem nicht vorhersehbaren Arbeitskontext; übernehmen die operative Verantwortung und planen, handeln und evaluieren autonom.

### Kompetenzniveau 4: Kompetenzexpertise

Entwickeln innovative Lösungen in einem komplexen Tätigkeitsfeld; antizipieren Veränderungen in der Zukunft und handeln proaktiv; übernehmen strategische Verantwortung und treiben Veränderungen und Entwicklungen voran.
