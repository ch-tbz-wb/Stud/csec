## Container Standards und Technologien

[[_TOC_]]

### Linux Namespaces

![](https://github.com/mc-b/duk/raw/master/images/linux-namespaces.png)

- - -

Ist die Technologie hinter den Containern.

Deren Aufgabe ist es die Ressourcen des Kernsystems (in diesem Falle also des Linux Kernels) voneinander zu isolieren.

**Arten von Namespaces**

* IPC -Interprozess-Kommunikation
* NET - Netzwerkressourcen
* PID -Prozess-IDs
* USER - Benutzer/Gruppen-Ids
* (UTS - Systemidentifikation): Über diesen Namespace kann jeder Container einen eigenen Host- und Domänennamen erhalten.

### Hands-on

* [Linux Namespaces oder die Linux Sicht auf Container](linuxns.md)

**Optional**:

* [Web-App](web-app.md)
* [Web-App mit Container](web-app-container.md)

### Links

* [DockerHub download rate limits](https://microk8s.io/docs/dockerhub-limits)