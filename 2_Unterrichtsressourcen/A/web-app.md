## Web-App ohne Container

Wir wollen die NodeJS App (Microservice) von [https://gitlab.com/ser-cal/Container-CAL-webapp_v1.git](https://gitlab.com/ser-cal/Container-CAL-webapp_v1.git) starten.

Für diese Übungen verwenden wir eine Standard Ubuntu Umgebung ohne Installierte Software.

Das Cloud-init Script sieht wie folgt aus:

    #cloud-config
    users:
      - name: ubuntu
        sudo: ALL=(ALL) NOPASSWD:ALL
        groups: users, admin
        home: /home/ubuntu
        shell: /bin/bash
        lock_passwd: false
        plain_text_passwd: 'insecure'        
    # login ssh and console with password
    ssh_pwauth: true
    disable_root: false    

Nachdem wir in die VM, z.B. via ssh, gewechselt haben, führen wir die folgenden Befehle aus:
    
Git Repository clonen und Versuch SW zu starten

    git clone https://gitlab.com/ser-cal/Container-CAL-webapp_v1.git
    cd Container-CAL-webapp_v1/App
    
    node app.js                                 # schlägt fehl, NodeJS etc. fehlt
    
Benötigte SW installieren 

    sudo apt-get update
    sudo apt-get install -y nodejs npm          # das sind 1 GB 
    
    npm install 
    
    node app.js &                               # das geht
    curl localhost:8080                         # geht auch
    
    node app.js                                 # Fehlermeldung Port 8080 schon verwendet
    
    > events.js:174
    > Error: listen EADDRINUSE: address already in use :::8080


**Fazit**: für eine so einfache Web-App wird (zu)viel Software (ca. 1 GB) installiert. Auch kann die App nicht mehrmals gestartet werden, weil der Port fix definiert ist.
