# Container Sicherheit

Im zweiten Teil gehen wir die Sicherheitsprobleme von Container an und deren Lösungen.

Dazu können wir die VM aus Beispiel [A](../A/) verwenden.

Unsicher Container Umgebung mit `docker`:

    docker run -v /:/homeroot -it ubuntu bash
    cat /homeroot/etc/sudoers # Funktioniert inkl. Änderung der System-Datei 

Sichere Container Umgebung mit `podman`:

    podman run -v /:/homeroot -it ubuntu bash
    cat /homeroot/etc/sudoers   # kein Zugriff!
    
    
