# Sichere Container Umgebung - PodMan

[[_TOC_]]

Im dritten Teil wechseln wir auf eine Sichere Container Umgebung wie PodMan.

Dazu arbeitet den RedHat Kurs [Red Hat OpenShift Developer I: Introduction to Containers with Podman](https://rha.ole.redhat.com/rha/app/courses/do188-4.12/) durch.

## Chapter 1, Introduction and Overview of Containers
* Describe how containers facilitate application development.
* Describe the basics of containers and how containers differ from Virtual Machines.
* Describe container orchestration and the features of Red Hat OpenShift.

## Chapter 2, Podman Basics
* Creating Containers with Podman
* Container Networking Basics
* Accessing Containerized Network Services
* Accessing Containers
* Managing the Container Lifecycle

**Zusatzaufgabe**:
* Startet Eure erstellten Microservices mit PodMan, wo sind die Unterschiede?
* Erstellt für die Datenbank ein eigenes Netzwerk, warum solltet Ihr das tun?

## Chapter 3, Container Images
* Navigate container registries to find and manage container images.
* Navigate container registries.
* Pull and manage container images.

**Zusatzaufgabe**:
* Analysiert Eure Container Images mit `podman inspect`. Sind keine sensitiven Daten wie Passwörter ersichtlich?
* Was ist an diesem [Beispiel](https://github.com/mc-b/duk/blob/master/data/jupyter/03-4-Docker.ipynb) alles schlecht?
* Welches System verwendet Ihr um Container Images zu taggen.
* Versucht Eure Container Images statt mit `docker build` mit [buildah](https://github.com/containers/buildah) zu erstellen.

## Chapter 4, Custom Container Images
* Build custom container images to containerize applications.
* Create a containerfile using basic commands.
* Create a containerfile using best practices.
* Run rootless containers with Podman.

**Zusatzaufgabe:**
* Überprüft Eure `Dockerfile`, entsprechen sie den best practices
* Entpackt Eurer Container-Image Layer für Layer und analysiert diese, siehe [hier](https://github.com/mc-b/duk/blob/master/data/jupyter/03-2-Docker.ipynb)
* Was macht dieses [Dockerfile](https://github.com/kubernetes/ingress-nginx/blob/main/rootfs/Dockerfile) besser als das Beispiel von RedHat? Ab Zeile 80.
 * **Selbststudium**: Problem mit Process-ID 1 und Lösung [dumb-init](https://github.com/Yelp/dumb-init).

## Chapter 5, Persisting Data
* Run database containers with persistence.
* Describe the process for mounting volumes and common use cases.
* Build containerized databases.

**Zusatzaufgabe:**
* Sind Eure Daten nach Zerstören des Containers noch vorhanden? 

## Chapter 6, Troubleshooting Containers
* Analyze container logs and configure a remote debugger.
* Read container logs and troubleshoot common container problems.
* Configure a remote debugger during application development.

**Zusatzaufgabe:**
* Probiert Eure Applikationscontainer zu debuggen

## Chapter 7, Multi-container Applications with Compose
* Run multi-container applications with Podman Compose.
* Describe compose and its common use cases.
* Configure a repeatable developer environment with Compose.

**Zusatzaufgabe:**
* Läuft Eure Applikation sowohl mit Docker und PodMan Compose?

## Chapter 8, Container Orchestration with OpenShift and Kubernetes
* Orchestrate containerized applications with Kubernetes and OpenShift.
* Deploy an application in OpenShift.
* Deploy a multi-pod application to OpenShift and make it externally available.

**Hinweis**:
* Kubernetes wird im 4. Semester ausführlich behandelt, Kapitel sollte weggelassen werden.

## Hands-On 

* [PodMan und verschiedene Linux Distributionen](wsl.md)

## Erweiterungen

* [Red Hat Extension Pack for Podman Desktop](https://github.com/redhat-developer/podman-desktop-redhat-pack-ext)