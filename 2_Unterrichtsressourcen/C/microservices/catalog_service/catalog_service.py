# catalog_service.py
from flask import Flask, jsonify, render_template_string
import socket
import setproctitle
from prometheus_client import start_http_server, Counter, Histogram, generate_latest
import time

setproctitle.setproctitle("catalog")
app = Flask(__name__)

products = [
    {"id": 1, "name": "Land Rover Range Rover", "price": 150000},
    {"id": 2, "name": "Land Rover Discovery", "price": 100000},
    {"id": 3, "name": "Land Rover Defender", "price": 120000},
    {"id": 4, "name": "BMW 3er", "price": 60000},
    {"id": 5, "name": "BMW X5", "price": 100000},
    {"id": 6, "name": "BMW i4", "price": 80000},
    {"id": 7, "name": "Audi A4", "price": 60000},
    {"id": 8, "name": "Audi Q5", "price": 70000},
    {"id": 9, "name": "Audi e-tron", "price": 100000},
    {"id": 10, "name": "Mercedes-Benz C-Klasse", "price": 70000},
    {"id": 11, "name": "Mercedes-Benz GLE", "price": 100000},
    {"id": 12, "name": "Mercedes-Benz EQS", "price": 140000},
    {"id": 13, "name": "Toyota Corolla", "price": 35000},
    {"id": 14, "name": "Toyota RAV4", "price": 45000},
    {"id": 15, "name": "Toyota Highlander", "price": 60000},
    {"id": 16, "name": "Tesla Model 3", "price": 55000},
    {"id": 17, "name": "Tesla Model Y", "price": 70000},
    {"id": 18, "name": "Tesla Model S", "price": 130000},
    {"id": 19, "name": "Ford Mustang", "price": 60000},
    {"id": 20, "name": "Ford Explorer", "price": 70000},
    {"id": 21, "name": "Ford F-150", "price": 70000},
    {"id": 22, "name": "Volkswagen Golf", "price": 45000},
    {"id": 23, "name": "Volkswagen Tiguan", "price": 50000},
    {"id": 24, "name": "Volkswagen ID.4", "price": 60000}
]

# Prometheus metrics
REQUEST_COUNT = Counter('catalog_service_request_count', 'App Request Count', ['endpoint'])
REQUEST_LATENCY = Histogram('catalog_service_request_latency_seconds', 'Request latency', ['endpoint'])

@app.route('/catalog/api', methods=['GET'])
def get_products():
    with REQUEST_LATENCY.labels('/catalog/api').time():
        REQUEST_COUNT.labels('/catalog/api').inc()
        return jsonify(products)

@app.route('/catalog', methods=['GET'])
def get_products_html():
    with REQUEST_LATENCY.labels('/catalog').time():
        REQUEST_COUNT.labels('/catalog').inc()
        hostname = socket.gethostname()
        return render_template_string('''
        <html>
        <head>
            <title>Catalog Service on {{hostname}}</title>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        </head>
        <body>
            <div class="container">
                <div class="jumbotron mt-4">
                    <h1 class="display-4">Catalog Service on {{hostname}}</h1>
                    <p class="lead">List of products</p>
                    <hr class="my-4">
                    <ul class="list-group">
                        {% for product in products %}
                            <li class="list-group-item">{{ product.name }} (${{ product.price }})</li>
                        {% endfor %}
                    </ul>
                </div>
            </div>
        </body>
        </html>
        ''', hostname=hostname, products=products)

@app.route('/metrics')
def metrics():
    return generate_latest(), 200

if __name__ == '__main__':
    start_http_server(8000)  # Prometheus server
    app.run(host='0.0.0.0', port=8080)
