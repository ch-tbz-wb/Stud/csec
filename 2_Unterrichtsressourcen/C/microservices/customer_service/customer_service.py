# customer_service.py
from flask import Flask, jsonify, render_template_string
import socket
import setproctitle
from prometheus_client import start_http_server, Counter, Histogram, generate_latest
import time

setproctitle.setproctitle("customer")

app = Flask(__name__)

customers = [
    {"id": 1, "name": "John Doe", "email": "john.doe@example.com"},
    {"id": 2, "name": "Jane Smith", "email": "jane.smith@example.com"},
    {"id": 3, "name": "Bob Johnson", "email": "bob.johnson@example.com"}
]

# Prometheus metrics
REQUEST_COUNT = Counter('customer_service_request_count', 'App Request Count', ['endpoint'])
REQUEST_LATENCY = Histogram('customer_service_request_latency_seconds', 'Request latency', ['endpoint'])

@app.route('/customer/api', methods=['GET'])
def get_customers():
    with REQUEST_LATENCY.labels('/customer/api').time():
        REQUEST_COUNT.labels('/customer/api').inc()
        return jsonify(customers)

@app.route('/customer', methods=['GET'])
def get_customers_html():
    with REQUEST_LATENCY.labels('/customer').time():
        REQUEST_COUNT.labels('/customer').inc()
        hostname = socket.gethostname()
        return render_template_string('''
        <html>
        <head>
            <title>Customer Service on {{hostname}}</title>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        </head>
        <body>
            <div class="container">
                <div class="jumbotron mt-4">
                    <h1 class="display-4">Customer Service on {{hostname}}</h1>
                    <p class="lead">List of customers</p>
                    <hr class="my-4">
                    <ul class="list-group">
                        {% for customer in customers %}
                            <li class="list-group-item">{{ customer.name }} ({{ customer.email }})</li>
                        {% endfor %}
                    </ul>
                </div>
            </div>
        </body>
        </html>
        ''', hostname=hostname, customers=customers)

@app.route('/metrics')
def metrics():
    return generate_latest(), 200

if __name__ == '__main__':
    start_http_server(8000)  # Prometheus server
    app.run(host='0.0.0.0', port=8080)
