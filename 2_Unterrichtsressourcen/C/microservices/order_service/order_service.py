# order_service.py
from flask import Flask, jsonify, render_template_string
import socket
import setproctitle
from prometheus_client import start_http_server, Counter, Histogram, generate_latest
import time

# Set the process title
setproctitle.setproctitle("order")

app = Flask(__name__)

# Example orders with complete details
orders = [
    {"id": 1, "product": "Land Rover Range Rover", "quantity": 1, "customer_name": "John Doe", "customer_id": 1, "product_id": 1},
    {"id": 2, "product": "Land Rover Discovery", "quantity": 2, "customer_name": "Jane Smith", "customer_id": 2, "product_id": 2},
    {"id": 3, "product": "Land Rover Defender", "quantity": 1, "customer_name": "Bob Johnson", "customer_id": 3, "product_id": 3}
]

# Prometheus metrics
REQUEST_COUNT = Counter('order_service_request_count', 'App Request Count', ['endpoint'])
REQUEST_LATENCY = Histogram('order_service_request_latency_seconds', 'Request latency', ['endpoint'])

@app.route('/order/api', methods=['GET'])
def get_orders():
    with REQUEST_LATENCY.labels('/order/api').time():
        REQUEST_COUNT.labels('/order/api').inc()
        return jsonify(orders)

@app.route('/order', methods=['GET'])
def get_orders_html():
    with REQUEST_LATENCY.labels('/order').time():
        REQUEST_COUNT.labels('/order').inc()
        hostname = socket.gethostname()
        return render_template_string('''
        <html>
        <head>
            <title>Order Service on {{hostname}}</title>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        </head>
        <body>
            <div class="container">
                <div class="jumbotron mt-4">
                    <h1 class="display-4">Order Service on {{hostname}}</h1>
                    <p class="lead">List of orders</p>
                    <hr class="my-4">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Customer Name</th>
                                <th>Customer ID</th>
                                <th>Product ID</th>
                            </tr>
                        </thead>
                        <tbody>
                            {% for order in orders %}
                                <tr>
                                    <td>{{ order.id }}</td>
                                    <td>{{ order.product }}</td>
                                    <td>{{ order.quantity }}</td>
                                    <td>{{ order.customer_name }}</td>
                                    <td>{{ order.customer_id }}</td>
                                    <td>{{ order.product_id }}</td>
                                </tr>
                            {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </body>
        </html>
        ''', hostname=hostname, orders=orders)

@app.route('/metrics')
def metrics():
    return generate_latest(), 200

if __name__ == '__main__':
    start_http_server(8000)  # Start Prometheus server to expose metrics
    app.run(host='0.0.0.0', port=8080)
