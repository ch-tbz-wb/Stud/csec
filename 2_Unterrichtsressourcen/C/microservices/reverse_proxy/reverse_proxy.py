from flask import Flask, request, Response, url_for
import requests
import socket
import os
import setproctitle
from prometheus_client import start_http_server, Counter, Histogram
import time

setproctitle.setproctitle("webshop")

app = Flask(__name__)

# Prometheus metrics
REQUEST_COUNT = Counter('webshop_request_count', 'App Request Count', ['endpoint'])
REQUEST_LATENCY = Histogram('webshop_request_latency_seconds', 'Request latency', ['endpoint'])

# Get prefix from environment variable
PREFIX = os.getenv('URL_PREFIX', '')

# Configuration for microservices
SERVICES = {
    'customer': f'http://customer{PREFIX}:8080',
    'catalog': f'http://catalog{PREFIX}:8080',
    'order': f'http://order{PREFIX}:8080'
}


@app.route(f'/{PREFIX}')
def index():
    hostname = socket.gethostname()
    customer_url = url_for('proxy_varied', service='customer', path='customer')
    catalog_url = url_for('proxy_varied', service='catalog', path='catalog')
    order_url = url_for('proxy_varied', service='order', path='order')
    
    return f'''
    <html>
    <head>
        <title>Reverse Proxy on {hostname}</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            <div class="jumbotron mt-4">
                <h1 class="display-4">Reverse Proxy on {hostname}</h1>
                <p class="lead">This is a simple reverse proxy example using Flask and Bootstrap.</p>
                <hr class="my-4">
                <ul class="list-group">
                    <li class="list-group-item"><a href="{customer_url}">Customer Service</a></li>
                    <li class="list-group-item"><a href="{catalog_url}">Catalog Service</a></li>
                    <li class="list-group-item"><a href="{order_url}">Order Service</a></li>
                </ul>
            </div>
        </div>
    </body>
    </html>
    '''

@app.route(f'/{PREFIX}/<service>/', defaults={'path': ''}, methods=['GET', 'POST', 'PUT', 'DELETE'])
@app.route(f'/{PREFIX}/<service>/<path:path>', methods=['GET', 'POST', 'PUT', 'DELETE'])
def proxy_varied(service, path):
    if service not in SERVICES:
        return Response(f"Service {service} not found", status=404)

    # Construct the URL to forward the request to
    service_url = f"{SERVICES[service]}/{path}"

    # Forward the request to the appropriate microservice
    if request.method == 'GET':
        response = requests.get(service_url, params=request.args)
    elif request.method == 'POST':
        response = requests.post(service_url, json=request.get_json())
    elif request.method == 'PUT':
        response = requests.put(service_url, json=request.get_json())
    elif request.method == 'DELETE':
        response = requests.delete(service_url)
    else:
        return Response("Method not supported", status=405)

    # Construct a Flask Response object from the response we got from the microservice
    excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
    headers = [(name, value) for name, value in response.raw.headers.items()
               if name.lower() not in excluded_headers]

    return Response(response.content, response.status_code, headers)

@app.route('/metrics')
def metrics():
    from prometheus_client import generate_latest
    return generate_latest(), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
