# Umsetzung

- Bereich: Datenschutz und Datensicherheit gewährleisten
- Semester: 3

## Lektionen

* Präsenzlektionen (Vor Ort): 10
* Präsenzlektionen (Distance Learning): 15
* Selbststudium: 15

### Fahrplan

| **Lektionen** | **Selbststudium** | **Inhalt**                                              | **Kompetenzenband** | **Tools**                |
|---------------|-------------------|---------------------------------------------------------|---------------------|--------------------------|
| 3             | 0                 | Linux Namespaces                                        | a)                  | Linux Bash |
| 4             | 1                 | Container Sicherheit                                    | b)                  | Docker, Podman |
| 4             | 2                 | Chapter 1: Introduction and Overview of Containers      | c)                  | Podman |
| 4             | 2                 | Chapter 2: Podman Basics                                | d)                  | Podman |
| 4             | 2                 | Chapter 3: Container Images                             | e)                  | Podman |
| 4             | 2                 | Chapter 4: Custom Container Images                      | f)                  | Podman |
| 4             | 2                 | Chapter 5: Persisting Data                              | g)                  | Podman |
| 4             | 2                 | Chapter 6: Troubleshooting Containers                   | h)                  | Podman |
| 4             | 2                 | Chapter 7: Multi-container Applications with Compose    | i)                  | Podman |
| **Total 35**  | **Total 15**      |   **-**                                                 |    **-**            |      **-**                |


## Voraussetzungen

* Microservices
* Container

## Dispensation 

* keine

## Methoden

Praktische Laborübungen mit Coaching durch Lehrperson

## Schlüsselbegriffe

* Container
* Sicherheit

## Lerninhalte

* Container Technologie
* Generelle Container Sicherheitsprobleme
* Einsatz sichere Container Umgebungen

## Übungen und Praxis

* Linux Namespaces
* Container Image analysieren
* NSA WhitePaper 
* PodMan Einführung

## Lehr- und Lernformen

Lehrervorträge, Lehrgespräche, Workshop, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen

## Lehrmittel

* Cloud Umgebung, z.B. AWS oder Private Cloud (MAAS.io)








