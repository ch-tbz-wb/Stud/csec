# Handlungsziele und Handlungssituationen 


## 1. Linux Namespaces

- Verstehen, wie Linux Namespaces zur Isolation von Prozessen verwendet werden.
- Die verschiedenen Arten von Namespaces und deren spezifische Funktionen kennen.
- Praktische Anwendung von Namespaces zur Verbesserung der Sicherheit und Effizienz von Containern.

**Typische Handlungssituation**: 
Anna, eine Systemadministratorin, erhält von ihrem Chef den Auftrag, sich mit Linux Namespaces vertraut zu machen, um die Isolation von Prozessen in Containern zu verbessern. Sie beginnt damit, die verschiedenen Arten von Namespaces wie PID, NET, MOUNT, IPC, UTS und USER zu studieren und deren spezifische Funktionen zu verstehen. Anna setzt ihr Wissen in die Praxis um, indem sie Namespaces konfiguriert, um Prozesse voneinander zu isolieren und die Sicherheit und Effizienz ihrer Container zu erhöhen. Sie dokumentiert ihre Erkenntnisse und teilt sie mit ihrem Team, um Best Practices zu etablieren.

## 2. Container Sicherheit

- Die grundlegenden Sicherheitsprinzipien für Container verstehen.
- Sicherheitsrisiken in Container-Umgebungen identifizieren und minimieren können.
- Best Practices zur Sicherung von Containern anwenden können.

**Typische Handlungssituation**: 
Anna wird beauftragt, die Sicherheit der Container-Umgebung ihres Unternehmens zu überprüfen und zu verbessern. Sie beginnt mit einer Analyse der aktuellen Sicherheitsmassnahmen und identifiziert potenzielle Schwachstellen. Anna lernt die grundlegenden Sicherheitsprinzipien für Container kennen, einschliesslich der Minimierung von Angriffsflächen, der Implementierung von Zugriffskontrollen und der Verwendung von sicheren Images. Sie wendet Best Practices an, um die Container-Umgebung zu sichern, und führt regelmässige Sicherheitsüberprüfungen durch. Anna erstellt einen Bericht über ihre Massnahmen und präsentiert diesen ihrem Chef, um die kontinuierliche Verbesserung der Container-Sicherheit sicherzustellen.

# PodMan

## 3. Chapter 1: Introduction and Overview of Containers

- Verstehen, wie Container die Anwendungsentwicklung erleichtern.
- Die Unterschiede zwischen Containern und virtuellen Maschinen kennen.
- Die Grundlagen der Container-Orchestrierung und die Funktionen von Red Hat OpenShift verstehen.

**Typische Handlungssituation**: 
Anna, eine Softwareentwicklerin, erhält von ihrem Chef den Auftrag, sich mit der Container-Technologie vertraut zu machen. Sie beginnt damit, die Grundlagen von Containern zu verstehen und wie diese die Anwendungsentwicklung erleichtern. Anna vergleicht Container mit virtuellen Maschinen und stellt fest, dass Container weniger Ressourcen benötigen und schneller starten. Ihr Chef möchte ausserdem, dass sie sich mit der Container-Orchestrierung und den Funktionen von Red Hat OpenShift auseinandersetzt, um zukünftige Projekte effizienter zu gestalten.

## 4. Chapter 2: Podman Basics

- Die grundlegende Nutzung von Podman zur Erstellung und Verwaltung von Containern erlernen.
- Die Grundlagen der Container-Netzwerke verstehen.
- Den Lebenszyklus von Containern managen können.

**Typische Handlungssituation**: 
Anna soll nun praktische Erfahrungen mit Podman sammeln. Sie erstellt ihre ersten Container und lernt, wie sie diese verwalten kann. Dabei stösst sie auf die Notwendigkeit, Container-Netzwerke zu konfigurieren, um auf verschiedene Dienste zugreifen zu können. Ihr Chef fordert sie auf, die Lebenszyklen der Container zu managen und ihre erstellten Microservices mit Podman zu starten. Anna bemerkt Unterschiede zu anderen Tools und erstellt ein eigenes Netzwerk für die Datenbank, um die Sicherheit und Effizienz zu erhöhen.

## 5. Chapter 3: Container Images

- Container-Images in verschiedenen Registries finden und verwalten können.
- Container-Images herunterladen und managen können.
- Sensible Daten in Container-Images identifizieren und vermeiden können.

**Typische Handlungssituation**: 
Anna wird beauftragt, Container-Images in verschiedenen Registries zu finden und zu verwalten. Sie navigiert durch die Registries, lädt Images herunter und verwaltet diese. Ihr Chef bittet sie, die Images mit `podman inspect` zu analysieren, um sicherzustellen, dass keine sensiblen Daten wie Passwörter enthalten sind. Anna überprüft ein Beispiel und erkennt Schwächen, die sie mit ihrem Chef diskutiert. Sie entwickelt ein System zur Kennzeichnung von Container-Images und versucht, diese mit Buildah zu erstellen, um die Effizienz zu steigern.

## 6. Chapter 4: Custom Container Images

- Benutzerdefinierte Container-Images zur Containerisierung von Anwendungen erstellen können.
- Ein Containerfile mit grundlegenden Befehlen und Best Practices schreiben können.
- Rootlose Container mit Podman ausführen können.

**Typische Handlungssituation**: 
Anna soll nun benutzerdefinierte Container-Images erstellen, um Anwendungen zu containerisieren. Sie schreibt ein Containerfile mit grundlegenden Befehlen und Best Practices. Ihr Chef möchte, dass sie rootlose Container mit Podman ausführt, um Sicherheitsrisiken zu minimieren. Anna überprüft ihre Dockerfiles auf Best Practices und analysiert die Container-Images Schicht für Schicht. Sie vergleicht ein Dockerfile-Beispiel mit dem von RedHat und diskutiert die Unterschiede mit ihrem Team. Schliesslich untersucht sie das Problem mit Process-ID 1 und findet eine Lösung mit dumb-init.

## 7. Chapter 5: Persisting Data

- Datenbank-Container mit Persistenz ausführen können.
- Den Prozess des Einbindens von Volumes und deren häufige Anwendungsfälle verstehen.
- Containerisierte Datenbanken erstellen können.

**Typische Handlungssituation**: 
Anna erhält den Auftrag, Datenbank-Container mit Persistenz auszuführen. Sie erklärt ihrem Team den Prozess des Einbindens von Volumes und deren häufige Anwendungsfälle. Anna erstellt containerisierte Datenbanken und überprüft, ob die Daten nach dem Zerstören des Containers noch vorhanden sind, um die Datenintegrität sicherzustellen.

## 8. Chapter 6: Troubleshooting Containers

- Container-Logs analysieren und einen Remote-Debugger konfigurieren können.
- Häufige Container-Probleme beheben können.
- Einen Remote-Debugger während der Anwendungsentwicklung konfigurieren können.

**Typische Handlungssituation**: 
Anna wird beauftragt, Container-Logs zu analysieren und einen Remote-Debugger zu konfigurieren. Sie liest und analysiert die Logs, um häufige Container-Probleme zu beheben. Ihr Chef fordert sie auf, während der Anwendungsentwicklung einen Remote-Debugger zu konfigurieren. Anna versucht, ihre Applikationscontainer zu debuggen und teilt ihre Erkenntnisse mit dem Team.

## 9. Chapter 7: Multi-container Applications with Compose

- Multi-Container-Anwendungen mit Podman Compose ausführen können.
- Die häufigen Anwendungsfälle von Compose verstehen.
- Eine wiederholbare Entwicklerumgebung mit Compose konfigurieren können.

**Typische Handlungssituation**: 
Anna soll Multi-Container-Anwendungen mit Podman Compose ausführen. Sie beschreibt ihrem Team die häufigen Anwendungsfälle von Compose und konfiguriert eine wiederholbare Entwicklerumgebung. Ihr Chef möchte sicherstellen, dass die Anwendung sowohl mit Docker als auch mit Podman Compose läuft, und Anna überprüft dies gründlich.

